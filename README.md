This application allows students to automatically start the Regie PPPoE 
connection when Windows starts up.

* Coding: Valentin-Gabriel Radu, 313CD, 1st Year (https://gitlab.com/valinet)
* Original idea & coding: Alexandru Lincan, 313CA, 2nd Year (https://github.com/fll0pY)

Installation Guide
__________________

1. Find the name of the Internet connection that you set up for the Regie PPPoE.
To do this, press Win+R, type "rasphone", and click Enter. Then, identify your 
connection in the list.

2. We will assume that our connection is called "Regie Net". In order to start
the application, we need to provide this name as a command line parameter.
Aditionally, we have to supply a number indicating the timeout for the ping
request we send to Google, in order to test the Internet connection. If unusure,
use our recommendation, which is 10. The command to launch our app would be:

    e.g. PPPoE_Start.exe "Regie Net" 10

Replace PPPoE_Start.exe with the full path towards our application.

Qucik Tip: Find this out wasily by right clicking the application executable,
and choosing "Copy as path".

3. Have the app start automatically as system startup. You could use a
scheduled task for this. Or place a shortcut in the good old Startup folder.
To do the latter, just right click the application executable, and choose
'Send to > Desktop (create shortcut)'. Now, go to the desktop, and right click 
the newly created shortcut, and choose 'Properties'. In the 'Target' field, just
add our command line parameters at the end of the line, as in the example above.

    e.g. .....tart.exe "Regie Net" 10
    
Click OK, Then, press Win+R, and type "shell:startup". This will open the 
"Startup" shell folder in Explorer. Everything you put in this folder gets run
when you log in to your computer. Now, just move the shortcut to the app, from
the desktop, to this directory.

You're all done, good luck!