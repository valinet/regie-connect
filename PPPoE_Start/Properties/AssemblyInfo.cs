﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Regie Connect")]
[assembly: AssemblyDescription("Regie Connect")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Regie, Inc.")]
[assembly: AssemblyProduct("Regie Connect")]
[assembly: AssemblyCopyright("Copyright © 2016 Regie, Inc. All rights reserved.")]
[assembly: AssemblyTrademark("Regie")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("17923ef8-d56c-4db5-84bb-43711474780d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.11.09.0")]
[assembly: AssemblyFileVersion("2016.11.09.0")]
