﻿/* Copyright (C) 2016 Valentin-Gabriel Radu, 313CD An I, implementare
 *                    Alexandru Lincan, 313CA An II, implemenatre, original idea & atmosfera
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

namespace PPPoE_Start
{
    public partial class Form1 : Form
    {
        #region pinvoke-calls-boring-stuff

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);
        const int WM_LBUTTONDOWN = 0x0201;
        const int WM_LBUTTONUP = 0x0202;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hWnd, uint msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "EnumDesktopWindows",
        ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool EnumDesktopWindows(IntPtr hDesktop, EnumDelegate lpEnumCallbackFunction, IntPtr lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        public delegate bool EnumDelegate(IntPtr hWnd, int lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);


        /// <summary>
        ///     Special window handles
        /// </summary>
        public enum SpecialWindowHandles
        {
            // ReSharper disable InconsistentNaming
            /// <summary>
            ///     Places the window at the top of the Z order.
            /// </summary>
            HWND_TOP = 0,
            /// <summary>
            ///     Places the window at the bottom of the Z order. If the hWnd parameter identifies a topmost window, the window loses its topmost status and is placed at the bottom of all other windows.
            /// </summary>
            HWND_BOTTOM = 1,
            /// <summary>
            ///     Places the window above all non-topmost windows. The window maintains its topmost position even when it is deactivated.
            /// </summary>
            HWND_TOPMOST = -1,
            /// <summary>
            ///     Places the window above all non-topmost windows (that is, behind all topmost windows). This flag has no effect if the window is already a non-topmost window.
            /// </summary>
            HWND_NOTOPMOST = -2
            // ReSharper restore InconsistentNaming
        }

        [Flags]
        public enum SetWindowPosFlags : uint
        {
            // ReSharper disable InconsistentNaming

            /// <summary>
            ///     If the calling thread and the thread that owns the window are attached to different input queues, the system posts the request to the thread that owns the window. This prevents the calling thread from blocking its execution while other threads process the request.
            /// </summary>
            SWP_ASYNCWINDOWPOS = 0x4000,

            /// <summary>
            ///     Prevents generation of the WM_SYNCPAINT message.
            /// </summary>
            SWP_DEFERERASE = 0x2000,

            /// <summary>
            ///     Draws a frame (defined in the window's class description) around the window.
            /// </summary>
            SWP_DRAWFRAME = 0x0020,

            /// <summary>
            ///     Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE is sent only when the window's size is being changed.
            /// </summary>
            SWP_FRAMECHANGED = 0x0020,

            /// <summary>
            ///     Hides the window.
            /// </summary>
            SWP_HIDEWINDOW = 0x0080,

            /// <summary>
            ///     Does not activate the window. If this flag is not set, the window is activated and moved to the top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter parameter).
            /// </summary>
            SWP_NOACTIVATE = 0x0010,

            /// <summary>
            ///     Discards the entire contents of the client area. If this flag is not specified, the valid contents of the client area are saved and copied back into the client area after the window is sized or repositioned.
            /// </summary>
            SWP_NOCOPYBITS = 0x0100,

            /// <summary>
            ///     Retains the current position (ignores X and Y parameters).
            /// </summary>
            SWP_NOMOVE = 0x0002,

            /// <summary>
            ///     Does not change the owner window's position in the Z order.
            /// </summary>
            SWP_NOOWNERZORDER = 0x0200,

            /// <summary>
            ///     Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent window uncovered as a result of the window being moved. When this flag is set, the application must explicitly invalidate or redraw any parts of the window and parent window that need redrawing.
            /// </summary>
            SWP_NOREDRAW = 0x0008,

            /// <summary>
            ///     Same as the SWP_NOOWNERZORDER flag.
            /// </summary>
            SWP_NOREPOSITION = 0x0200,

            /// <summary>
            ///     Prevents the window from receiving the WM_WINDOWPOSCHANGING message.
            /// </summary>
            SWP_NOSENDCHANGING = 0x0400,

            /// <summary>
            ///     Retains the current size (ignores the cx and cy parameters).
            /// </summary>
            SWP_NOSIZE = 0x0001,

            /// <summary>
            ///     Retains the current Z order (ignores the hWndInsertAfter parameter).
            /// </summary>
            SWP_NOZORDER = 0x0004,

            /// <summary>
            ///     Displays the window.
            /// </summary>
            SWP_SHOWWINDOW = 0x0040,

            // ReSharper restore InconsistentNaming
        }

        enum ShowWindowCommands
        {
            /// <summary>
            /// Hides the window and activates another window.
            /// </summary>
            Hide = 0,
            /// <summary>
            /// Activates and displays a window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position.
            /// An application should specify this flag when displaying the window 
            /// for the first time.
            /// </summary>
            Normal = 1,
            /// <summary>
            /// Activates the window and displays it as a minimized window.
            /// </summary>
            ShowMinimized = 2,
            /// <summary>
            /// Maximizes the specified window.
            /// </summary>
            Maximize = 3, // is this the right value?
                          /// <summary>
                          /// Activates the window and displays it as a maximized window.
                          /// </summary>       
            ShowMaximized = 3,
            /// <summary>
            /// Displays a window in its most recent size and position. This value 
            /// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except 
            /// the window is not activated.
            /// </summary>
            ShowNoActivate = 4,
            /// <summary>
            /// Activates the window and displays it in its current size and position. 
            /// </summary>
            Show = 5,
            /// <summary>
            /// Minimizes the specified window and activates the next top-level 
            /// window in the Z order.
            /// </summary>
            Minimize = 6,
            /// <summary>
            /// Displays the window as a minimized window. This value is similar to
            /// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowMinNoActive = 7,
            /// <summary>
            /// Displays the window in its current size and position. This value is 
            /// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowNA = 8,
            /// <summary>
            /// Activates and displays the window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position. 
            /// An application should specify this flag when restoring a minimized window.
            /// </summary>
            Restore = 9,
            /// <summary>
            /// Sets the show state based on the SW_* value specified in the 
            /// STARTUPINFO structure passed to the CreateProcess function by the 
            /// program that started the application.
            /// </summary>
            ShowDefault = 10,
            /// <summary>
            ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
            /// that owns the window is not responding. This flag should only be 
            /// used when minimizing windows from a different thread.
            /// </summary>
            ForceMinimize = 11
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left, Top, Right, Bottom;

            public RECT(int left, int top, int right, int bottom)
            {
                Left = left;
                Top = top;
                Right = right;
                Bottom = bottom;
            }

            public RECT(System.Drawing.Rectangle r) : this(r.Left, r.Top, r.Right, r.Bottom) { }

            public int X
            {
                get { return Left; }
                set { Right -= (Left - value); Left = value; }
            }

            public int Y
            {
                get { return Top; }
                set { Bottom -= (Top - value); Top = value; }
            }

            public int Height
            {
                get { return Bottom - Top; }
                set { Bottom = value + Top; }
            }

            public int Width
            {
                get { return Right - Left; }
                set { Right = value + Left; }
            }

            public System.Drawing.Point Location
            {
                get { return new System.Drawing.Point(Left, Top); }
                set { X = value.X; Y = value.Y; }
            }

            public System.Drawing.Size Size
            {
                get { return new System.Drawing.Size(Width, Height); }
                set { Width = value.Width; Height = value.Height; }
            }

            public static implicit operator System.Drawing.Rectangle(RECT r)
            {
                return new System.Drawing.Rectangle(r.Left, r.Top, r.Width, r.Height);
            }

            public static implicit operator RECT(System.Drawing.Rectangle r)
            {
                return new RECT(r);
            }

            public static bool operator ==(RECT r1, RECT r2)
            {
                return r1.Equals(r2);
            }

            public static bool operator !=(RECT r1, RECT r2)
            {
                return !r1.Equals(r2);
            }

            public bool Equals(RECT r)
            {
                return r.Left == Left && r.Top == Top && r.Right == Right && r.Bottom == Bottom;
            }

            public override bool Equals(object obj)
            {
                if (obj is RECT)
                    return Equals((RECT)obj);
                else if (obj is System.Drawing.Rectangle)
                    return Equals(new RECT((System.Drawing.Rectangle)obj));
                return false;
            }

            public override int GetHashCode()
            {
                return ((System.Drawing.Rectangle)this).GetHashCode();
            }

            public override string ToString()
            {
                return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{{Left={0},Top={1},Right={2},Bottom={3}}}", Left, Top, Right, Bottom);
            }
        }
        #endregion

        #region little-helper-methods
        public static string GetText(IntPtr hWnd)
        {
            /*
             * Returns the text of a window, by providing an hWnd to it
             */
            int length = GetWindowTextLength(hWnd);
            StringBuilder sb = new StringBuilder(length + 1);
            GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        bool IsEthernetConnected()
        {
            /*
             * Checks whether we have a cable plugged into the Ethernet jack.
             */
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces().Where(n => n.OperationalStatus == OperationalStatus.Up))
            {
                if (nic.Name == "Ethernet")
                    return true;
                //Console.WriteLine(nic.Name + "\t" + nic.);
            }
            return false;
        }

        void CheckRunningCoditions(string value)
        {
            /*
			 * If Ethernet is connected, we check to see whether we can reach the Internet; if not, we
			 * do the job - in any other case, we just terminate
			 */
            if (IsEthernetConnected())
            {
                Ping ping = new Ping();
                PingReply pr = ping.Send("8.8.8.8", Convert.ToInt32(value));
                if (pr.Status == IPStatus.Success)
                    Environment.Exit(0);
            }
            else
                Environment.Exit(0);
        }
        #endregion

        void Connect()
        {
            /*
			 * Most of the code examples courtesy of pinvoke.net, and stackoverflow.com
			 */

            /*
			 * Rasphone.exe is the Windows GUI utility that performs a connection. I think it calls the
			 * command line utility rasconnect to do this, as well. We used this in order to avoid having
			 * to code a new UI for the app. Type Rasphone.exe /? in cmd to display a list of the supported
			 * command line parameters.
			 */
            Process p = Process.Start(@"C:\Windows\system32\rasphone.exe", "-d \"" + cmds[1] + "\"");

            /*
			 * By convention, this will hold the hWnd of the foreground window.
			 */
            IntPtr fWnd = IntPtr.Zero;

            /*
			 * This specifies whether this is the first call of the delegate bellow (false), or the second (true).
			 */
            bool step = false;

            /* 
			 * Filters the results of the EnumDesltopWindows, looking for the main connection window of
			 * Rasphone if step = false, and then for the little popup window that is displayed when connecting, 
			 * when step = true.
			 */
            EnumDelegate filter = delegate (IntPtr hWnd, int lParam)
            {
                StringBuilder strbTitle = new StringBuilder(255);
                int nLength = GetWindowText(hWnd, strbTitle, strbTitle.Capacity + 1);
                string strTitle = strbTitle.ToString();

                string text = "Connect " + cmds[1];
                if (step)
                {
                    text = "Connecting to " + cmds[1] + "...";
                }
                if (IsWindowVisible(hWnd) && string.IsNullOrEmpty(strTitle) == false && strTitle == text)
                {
                    fWnd = hWnd;
                    return true;
                }
                return true;
            };

            /* 
			 * Waits for Rasphone to open the main window
			 */
            while (fWnd == IntPtr.Zero)
            {
                if (!EnumDesktopWindows(IntPtr.Zero, filter, IntPtr.Zero))
                {
                    Environment.Exit(2);
                }
            }

            /* 
			 * Hides the Rasphone window, without actually destroying it, so we can still programatically press 'Connect'.
			 */
            SetWindowPos(fWnd, (IntPtr)SpecialWindowHandles.HWND_TOP, 0, 0, 0, 0, SetWindowPosFlags.SWP_SHOWWINDOW);

            /* 
			 * We'll get a list of child windows of Rasphone's main window, so that we can find an hWnd for the
			 * 'Connect' button.
			 */
            List<IntPtr> allChildWindows = new WindowHandleInfo(fWnd).GetAllChildHandles();

            IntPtr connecthWnd = IntPtr.Zero;
            foreach (IntPtr i in allChildWindows)
            {
                if (GetText(i) == "&Connect")
                {
                    connecthWnd = i;
                    break;
                }
            }

            /*
			 * We found the hWnd, we now want to press it.
			 */
            SendMessage(connecthWnd, WM_LBUTTONDOWN, 0, IntPtr.Zero);
            SendMessage(connecthWnd, WM_LBUTTONUP, 0, IntPtr.Zero);

            /*
			 * We need to get an hWnd to the little window that shows the connection progress, so that we can display it
			 * in the lower right corner of the screen, the same way a Windows 10 notification is displayed.
			 */
            step = true;

            fWnd = IntPtr.Zero;
            while (fWnd == IntPtr.Zero)
            {
                if (!EnumDesktopWindows(IntPtr.Zero, filter, IntPtr.Zero))
                {
                    Environment.Exit(4);
                }
            }

            /*
			 * We get the coordinates of the small window, and set its new position on screen.
			 */
            RECT rct;
            var windowRec = GetWindowRect(fWnd, out rct);
            SetWindowPos(fWnd, (IntPtr)SpecialWindowHandles.HWND_TOP, Screen.PrimaryScreen.WorkingArea.Width - rct.Width,
                Screen.PrimaryScreen.WorkingArea.Bottom - rct.Height, rct.Width, rct.Height,
                SetWindowPosFlags.SWP_SHOWWINDOW);

            /* 
			 * We set the little window as the foreground window
			 */
            SetForegroundWindow(fWnd);

            /*
			 * Stays in this method while Rasphone.exe finishes its job
			 */
            while (IsWindowVisible(fWnd)) ;

        }

        void Disconnect()
        {
            /*
			 * Disconnects the PPPoE connection
			 */
            Process.Start(@"C:\Windows\system32\rasphone.exe", "-h \"" + cmds[1] + "\"");
        }

        string[] cmds;
        public Form1()
        {
            /*
			 * Standard WinForms call
			 */
            InitializeComponent();

            /*
			 * Checks for parameters, the app won't run without
             * 
             * The app should be called like this: 
             * PPPoE_Start.exe "Regie Net" 10 - Starts the application; the PPPoE connection called 'Regie Net'
             * will be 'dialed', with a timeout of 10ms when pinging Google when testing Internet connectivity.
			 */
            cmds = Environment.GetCommandLineArgs();

            /* 
			 * If no parameters supplied, terminate immediatly
			 */
            if (cmds.Length == 1)
            {
                Environment.Exit(1);
            }

            /*
			 * Performs Internet connection checks, in order to see whether it is actually necessary to perform the job.
			 */
            CheckRunningCoditions(cmds[2]);

            /*
			 * For some reason, connecting once at startup does not work, but we observed that the following did the trick.
			 */
            Connect();
            Disconnect();
            Connect();

            /*
			 * Die
			 */
            Environment.Exit(0);
        }
    }
}
